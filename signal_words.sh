#!/bin/bash
#set file location
#this script uses the output of ssh.sh as input
file="tweets/output.txt"

#search for the signal words in the text file with tweets
#output text file for each signal word and the tweet it was found in
cat $file | grep -i '\<echter\>' > files/echter.txt
cat $file | grep -i '\<hoewel\>' > files/hoewel.txt
cat $file | grep -i '\<integendeel\>' > files/integendeel.txt
cat $file | grep -i '\<anderzijds\>' > files/anderzijds.txt
