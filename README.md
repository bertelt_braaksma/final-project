#Final Project
This repository was created for the course Inleiding wetenschappelijk onderzoek (LIX024P05).

#Contents
##Main folder
The bash script ssh.sh is used to obtain the tweets from the RuG server and put them on your local machine. It will only work if you have acces to this particular server. You can also test the scripts using the sample tweets provided in output.txt in the tweets folder. We have provided the script run.sh, which runs signal_words.sh and data_to_csv.py for you so you can easily process the data in the same way that we did. The R-script chi-squared.r does statistical calculations on the data. 
##Files folder
The signal_words.sh script detects signal words in tweets and then writes the tweet to the appropriate text file. Before running this script, this folder contains placeholder files.
##Tweets folder
Once you have retrieved the tweets using the ssh.sh script, the output.txt file should be placed in this folder. For the purpose of demonstration, the file in this folder contains sample tweets.