#this script takes text-files created by the signal_words bash script as input
#it then identifies signal words and tweet length and then outputs a csv-file

import csv

#open csv-file to write data to
with open("data.csv", "w") as csvfile:
    fieldnames = ["signal_word", "length"]
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

    writer.writeheader()
    
    #create tuple with signal words
    variables = ("echter", "hoewel", "integendeel", "anderzijds")

    for word in variables:
      #open text file for each signal word
      txt = open("files/" + word + ".txt")
      for tweet in txt:
        #define short tweets as less than 100 characters
        if len(tweet) < 100:
          writer.writerow({"signal_word": word, "length": "short"})
        else:
          writer.writerow({"signal_word": word, "length": "long"})
