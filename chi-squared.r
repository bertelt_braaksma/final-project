library(vcd)

#import dataset
dat <- read.csv(file="data.csv", header=TRUE, sep=",")
str(dat)
tab <- table(dat)

#perform chi-squared test
chi <- chisq.test(tab)

#show frequencies
chi$observed
chi$expected
chi

#show effect size
assocstats(tab)$cramer