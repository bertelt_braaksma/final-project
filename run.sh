#!/bin/bash
echo This script processes the Twitter data and creates a .csv file.
echo Right now, it is looking for the signal words.
sh signal_words.sh
echo The program has found the signal words and is now creating a .csv file
python3 data_to_csv.py
echo Done!
